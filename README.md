The NOWA School 

Open and reproducible science entails the use of open software, provenance tracking of the project, quality assurance of the code, and proper research data management (RDM). [NOWA](https://sfbs.pages.uni-marburg.de/sfb135/nowa/nowa.site/) (Neuroscientific Workflow Assistance) is the infrastructure project of the [CRC/TRR 135](https://www.sfb-perception.de/) "Cardinal Mechanisms of Perception".

The idea is to offer the trainings within one week, as the modules taught go hand in hand and we hope for a more holistic experience compared to having single workshops spread over a few months. Modules of the NOWA School are:

- Research Data Management (RDM)
- Psychopy
- Git & GitLab
- Python & Jupyter
- Clean code & automated code testing (CI/CD)

The modules are taught by an example project on which we will work together over the whole week. After we learned about Research Data Management (RDM) standards and techniques (Monday), we will develop an experiment for our example project in Psychopy (Tuesday). On Wednesday, we will learn how to version control this code with Git and how to work on it collaboratively on GitLab. For our example project, we will also need to do some statistical analyses and figures, using Python (Thursday). All of our code should be tested, be clean, and re-usable (Friday).

The NOWA school was first held on February 12th-16th 2024.

All materials are published as a [JupyterBook](https://julia-pfarr.gitlab.io/nowaschool). 

____
Initial template for the JupyterBook forked from @NikosAlexandris -- Thank you @stingrayza
