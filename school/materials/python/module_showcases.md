# Python module showcases

The reality of courses like this one is that they really can't teach attending folks all they need to know to subsequently apply the adjacent content sufficiently, not to mention the training aspect. At best, courses like this one provide folks with the materials, overview and pointers necessary to continue and actually dive into the taught topics (the truth hurts sometimes, doesn't it?). Another important aspect that is frequently overlooked is that attendees usually need a certain set of skills and experience to really benefit from a given course, especially considering the diverse backgrounds folks have. We want to be fair, open and welcoming to everyone and while we know that we unfortunately can't achieve and assume a truly similar level of training and experience across all participants, we can at least do our best to work towards certain opportunities. That being said, courses like the one you're currently looking at, quite often (should) provide add-on materials covering and teaching basic to advanced programming skills applied to `file handling`/`input`/`output`, `signal processing`, `statistics`, `linear algebra` (`vectors` much?) and certain `data modalities`. 

As we unfortunately can't talk about all the cool things out there wrt `analyzes`, etc. and everyone works on different projects with different `data` and `aims`, we decided to leave the last part of this session open and provide a couple of showcases, providing an overview/introduction to a broad collection of `python modules`, including "general" and "specialized" applications.

Thus, please have a look at the different `materials` provided [here](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/showcases) and check out what interests you. Please let us know if you have any questions.

[Introduction to NumPy](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/python_numpy.html)

[Introduction to SciPy](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/python_scipy.html)

[Introduction to scikit-learn & scikit-image](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/python_scikit.html)

[Visualization for data in python](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/python_visualization_for_data.html)

[Introduction to Neuroimaging in python - Nibabel](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/image_manipulation_nibabel.html)

[Introduction to Neuroimaging in python - Nilearn](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/image_manipulation_nilearn.html)

[Introduction to fMRI analyses in python](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/statistical_analyses_MRI.html)

[Introduction to fMRI functional connectivity in python](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/functional_connectivity.html)

[Introduction to diffusion imaging in python](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/diffusion_imaging.html)

[MNE python for EEG - introduction](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/mne_introduction.html)

[MNE python for EEG - preprocessing](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/mne_preprocessing.html)

[MNE python for EEG - a typical workflow](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/mne_workflow_tutorial.html)

[Neuroimaging - Machine learning - preparation](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/machine_learning_preparation.html)

[Neuroimaging - Machine learning - Nilearn](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/machine_learning_nilearn.html)

[Neuroimaging - Machine learning - Tensorflow](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/DL_build_train.html)

[Neuroimaging - Machine learning - predicting age with scikit-learn](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases/Predicting_age_with_machine_learning.html)



## Hakuna matata
We can't stress this enough: it's all ok, don't let the imposter syndrome get the better of you.

![logo](https://c.tenor.com/yfDp5K_0_LMAAAAC/you-got-this-thumbs-up.gif)\
<sub><sup><sub><sup>https://c.tenor.com/yfDp5K_0_LMAAAAC/you-got-this-thumbs-up.gif
</sup></sub></sup></sub>

